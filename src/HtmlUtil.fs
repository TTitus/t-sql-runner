﻿module HtmlUtil

open System.Web

let htmlEncode x = HttpUtility.HtmlEncode x

let html body css js =
    sprintf 
        """
        <!-- https://codepen.io/capnhairdo/pen/qBaGRBV -->
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="utf-8" />
            <meta http-equiv="x-ua-compatible" content="ie=edge" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <title>SQL OUTPUT</title>
            <style>
            %s
            </style>
        </head>
        <body>

        <div id="page">
           <div id="chartWrapper">
            %s
          </div>
        </div>

        <script>
        %s
        </script>
        </body>
        </html>
        """
        css
        body
        js

let jsData = """
var chartTable   = $('table#chart'),
        chartWrapper = chartTable.parent(),
        chartWidth  = chartTable.width(),
        chartHeight = chartTable.height();

/* This could be tweaked further so that it makes the chart only as wide
   as the viewport if smaller than the page width. */
function sizeChart() {
    /* First, make the wrapper no wider than the page. Start by hiding the
     chart so it can't push out the page width, but fix the height of the
     wrapper temporarily so things don't bounce around. Then measure the
     width of the wrapper without the chart, which should fill the width of
     its container. Set this value as the wrapper's max-width, then show
     the chart again and unset the wrapper height. Only show the scrollbars
     if necssary. */
    chartWrapper.css({
        height   : chartWrapper.height(),
        maxWidth : ''
    });
    chartTable.hide();
    var maxWidth = chartWrapper.width();
    chartWrapper.css({
        maxWidth  : maxWidth,
        height    : '',
        overflowX : chartWidth > maxWidth ? 'auto' : 'visible'
    });
    chartTable.show();

    /* Next, make the wrapper no taller than the viewport. Do this by measuring
     the height of the viewport, then subtracting the offset of the first row
     (not header, which we assume is sticky). Only show the scrollbars if
     necessary. */
    var viewportHeight = $(window).height(), //same as document.documentElement.clientHeight
            firstRowOffset = chartTable.find('td:first')[0].getBoundingClientRect().top || 0;
    if (firstRowOffset > viewportHeight) {
        // chart is off the bottom of the screen, so no need for maxHeight
        maxHeight = '';
    } else if (firstRowOffset < 0) {
        // top of chart is off the top of the screen, so make the chart the same
    // height as the viewport
        maxHeight = viewportHeight;
    } else {
        // chart is on screen, so make it fit entirely within the viewport, but no
    // less than 150px high
        maxHeight = Math.max(viewportHeight - firstRowOffset, 150);
    }
    chartWrapper.css({
        maxHeight : maxHeight,
        overflowY : chartHeight > maxHeight ? 'auto' : 'visible'
    });
}

function throttle (callback, limit) {
    var waiting = false;                      // Initially, we're not waiting
    return function () {                      // We return a throttled function
        if (!waiting) {                       // If we're not waiting
            callback.apply(this, arguments);  // Execute users function
            waiting = true;                   // Prevent future invocations
            setTimeout(function () {          // After a period of time
                waiting = false;              // And allow future invocations
            }, limit);
        }
    }
}

// Now run it, but not too often.
// Consider also adding a textresize event: https://github.com/jney/jquery-textresize-event
$(window).on('load resize scroll', throttle(sizeChart, 1000));
"""

let cssData = """
body {
	font-family: 'Helvetica', 'Arial', sans-serif;
    font-size: 14pt;
}
div#page {
  max-width:100%; /* just to demonstrate horizontal scrolling & stickiness */
}
table#chart {
	width:3000px; /* just to demonstrate horizontal scrolling & stickiness */
	margin:0;
	border:none;
	/* background-color:white; */
	border-collapse:separate;
	border-spacing:0;
	border-left:1px solid black;
}
td {
	white-space: nowrap;
}
th {
	white-space: nowrap;
}
table#chart th {
	background:black;
	border-right:1px solid white;
	color:white;
	padding:2px 4px;
	position:sticky;
	top:0;
  /* ensure header row sits atop everything else when scrolling down */
	z-index:1;
}
table#chart td {
	/*background:white;*/
	border-right:1px solid black;
	border-bottom:1px solid black;
	padding:2px 4px;
}
/* ensure first header cell sits atop everything else when scrolling right */
table#chart th:first-child {
	position:sticky;
	border-right: 3px solid white;
	left:0;
	z-index:3;
}
/* make first column sticky when scrolling right */
table#chart td:first-child {
	position:sticky;
	left:0;
	border-right: 3px solid black;
	z-index:2;
	background: white;
}
tr:nth-child(even) td:first-child {background: 	#E8E8E8 !important}
tr:nth-child(odd) td:first-child  {background: white !important}
tr:nth-child(even) {background: 	#E8E8E8 !important}
tr:nth-child(odd) {background: white !important}
"""

let createHtml htmlBody = html htmlBody cssData jsData


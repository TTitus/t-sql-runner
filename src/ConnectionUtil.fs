﻿module ConnectionUtil

open System.Data.SqlClient
open System.IO
open System

type ActiveConnection = {
    connection : SqlConnection option
    connectionString : string
    server : string
    db : string
    openedAt : DateTime
}

let connIsOpen (c: SqlConnection option) =
    match c.IsSome with
    | true ->
        match c.Value.State with
        | Data.ConnectionState.Open -> true
        | _ -> false
    | false -> false

let connect connectionString = 
    let connection = new SqlConnection(connectionString)
    connection.Open()
    connection

let createConnectionString (server: string) (db: string) = sprintf "Data Source=%s;Initial Catalog=%s;Integrated Security=True" server db

let createNewConnection (server: string) (db: string) createNewConnection =
    let s = (server.ToLower())
    let d = (db.ToLower())
    let connectionString = createConnectionString s d
    let conn = 
        match createNewConnection with 
        | true -> Some (connectionString |> connect)
        | false -> None
    {
        connection = conn;
        connectionString = connectionString;
        server = s;
        db = d;
        openedAt = DateTime.Now;
    }

let closeConnection (conn: SqlConnection) =
    conn.Close()

let getConnectionInfoFromConfigFile connConfigFilePath =
    let lines = File.ReadAllLines connConfigFilePath
    // by convention, the first line is the server, the second line the db
    let server = lines[0]
    let db = lines[1]
    createNewConnection server db false

let includeNewConnection(activeConnections, requestedConn) =
    let existingConn = 
        activeConnections
        |> List.filter (fun (x: ActiveConnection) -> 
            x.db.Equals(requestedConn.db) && x.server.Equals(requestedConn.server)) 
        |> List.tryExactlyOne

    if Option.isNone existingConn then
        let x = createNewConnection requestedConn.server requestedConn.db true
        (x.connection, x :: activeConnections)
    else 
        (existingConn.Value.connection, activeConnections)

let purgeBadConnections activeConnections = List.filter (fun x -> connIsOpen x.connection) activeConnections

let processQueryRequest queryfilePath serverDbConfigFilePath activeConnections =
    if File.Exists(queryfilePath) then
        let queryText = File.ReadAllText(queryfilePath)
        let newConn = 
            serverDbConfigFilePath
            |> getConnectionInfoFromConfigFile

        let (conn, newActiveConnList) = includeNewConnection(activeConnections, newConn)

        File.Delete(queryfilePath)
        (conn, queryText, newActiveConnList)
    else
        (None, "", activeConnections)

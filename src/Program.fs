﻿open System.IO

// INPUT
let home = System.Environment.GetFolderPath(System.Environment.SpecialFolder.UserProfile) 
let root = Path.Combine(home, "SqlRunner", "files")
let queryfilePath = Path.Combine(root, "query.sql")
let serverDbConfigFilePath = Path.Combine(root, "serverDbConfig.txt")
let indexHtmlOutputPath = Path.Combine(root, "index.html")

let (userConfig: ConfigUtil.UserConfig) = {
    queryfilePath = queryfilePath;
    serverDbConfigFilePath = serverDbConfigFilePath;
    indexHtmlOutputPath = indexHtmlOutputPath
    throttleMilliseconds = 1000;
}

// RUN PROGRAM
if(File.Exists(queryfilePath)) then File.Delete(queryfilePath)
QueryUtil.sqlQueryReplFileWatcher [] userConfig

﻿module QueryUtil

open System.Data.SqlClient
open System.IO
open System

let rec sqlDataReaderProcess (reader: SqlDataReader) =

    let processOneResult (reader: SqlDataReader) =
        if (not reader.IsClosed) && (reader.HasRows) then
            let maxIndex = reader.FieldCount - 1
            let colIndexes = [0..maxIndex]

            let rec processRows (r: SqlDataReader) (colIndexes: int list) (result: string) =
                if r.Read() then

                    let row = colIndexes |> List.map (fun i -> 
                        HtmlUtil.htmlEncode ($"{r.GetValue(i)}".Replace("\t", "<TAB>"))
                    )
                    let pre = """<tr><td>"""
                    let d = List.reduce (fun x y -> x + """</td><td>""" + y) row
                    let post = """</td></tr>""" 
                    let strRow = pre + d + post
                    processRows r colIndexes (result + Util.newline + strRow)
                else
                    result

            let headersTmp =  
                colIndexes 
                |> List.map (
                    fun i -> HtmlUtil.htmlEncode (
                        $"{reader.GetName(i)}".Replace("\t", "<TAB>")
                    ) 
                )
                |> List.reduce (fun x y -> x + """</th><th>""" + y)

            let hpre = """<thead><tr><th>"""
            let hpost = """</th></tr></thead>""" 
            let headers = hpre + headersTmp + hpost

            let datarows = "<tbody>" + (processRows reader colIndexes "") + "</tbody>"
            let data = (
                "<table id='chart'>" + Util.newline 
                + headers + Util.newline 
                + datarows + Util.newline
                + "</table>"
            )

            //if reader.NextResult() then
            //    printfn "ERROR - need to handle next result"
            //else
            //    printfn "OKAY - no more results"

            (reader.NextResult(), reader, data)
        else
            (false, null, null)

    let rec processAllResults theReader resultData hasNextResult =
        if hasNextResult then
            let (newHasNextResult, newReader, newData) = processOneResult theReader
            let combinedData = (resultData + Util.newline + "<br><br>" + Util.newline + newData)
            processAllResults newReader combinedData newHasNextResult
        else
            resultData

    let (hasNextResult: bool) = (not reader.IsClosed) && (reader.HasRows)
    processAllResults reader "" hasNextResult

let nonquery (conn: SqlConnection) (cmd: string) =
    use sqlcmd = new SqlCommand(cmd, conn)
    let qtyRowsAffected = sqlcmd.ExecuteNonQuery()
    qtyRowsAffected

let query (conn: SqlConnection) (cmd: string) =
    use sqlcmd = new SqlCommand(cmd, conn)
    use reader = sqlcmd.ExecuteReader()
    reader |> sqlDataReaderProcess |> HtmlUtil.createHtml

let runquery(queryString, conn, indexHtmlOutputPath) =
    let (result: string) = query conn queryString
    File.WriteAllText(indexHtmlOutputPath, result)

//let r1 = SqlUtil.nonquery conn query
let runnonquery() = failwith "not implemented"

let rec sqlQueryReplFileWatcher activeConnections (userConfig: ConfigUtil.UserConfig) =
    
    let (conn, queryText, newActiveConnections) = 
        ConnectionUtil.processQueryRequest
            userConfig.queryfilePath 
            userConfig.serverDbConfigFilePath 
            (ConnectionUtil.purgeBadConnections activeConnections)

    Console.Clear()
    printfn "\nActive Connections:"
    newActiveConnections |> List.map (fun x -> 
        printfn "%s" (
            (Util.padRight $"Server: {x.server}" " " 35)
            + (Util.padRight $"Db: {x.db}" " " 30)
            + (Util.padRight $"Uptime (mins): {Util.dateDiffMins DateTime.Now x.openedAt}" " " 20)
        )
    ) |> ignore

    if(ConnectionUtil.connIsOpen conn) then
        runquery(queryText, conn.Value, userConfig.indexHtmlOutputPath)

    Threading.Thread.Sleep(userConfig.throttleMilliseconds)
    sqlQueryReplFileWatcher newActiveConnections userConfig






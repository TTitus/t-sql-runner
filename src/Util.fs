﻿module Util

open System.IO
open System

let printMap (map: Map<string, string>) =
    map |> Map.map (fun n s -> printfn "%s: %s" n s) |> ignore

let multiSplit (sep: char[]) (str: string) = 
    str.Split(sep)
    |> Array.map (fun x -> x.Trim()) 
    |> Array.toSeq

let split (sep: char) (str: string) = multiSplit (Array.singleton sep) str

let show (x: string) = printfn "%s" x

let mapAdd (map: Map<string, string>) colName value = map.Add(colName, value)

let linesToStr xs = Array.reduce (fun x y -> x + System.Environment.NewLine + y) xs

let newline = System.Environment.NewLine

let readAllLines x = File.ReadAllLines(x)

let rec padRight (str: string) (pad: string) len =
    if String.length str < len then
        padRight (str + pad) pad len
    else
        str

//let dateDiffMins (a: DateTime) (b: DateTime) = sprintf "%.2f" ((a - b).TotalMinutes)
let dateDiffMins (a: DateTime) (b: DateTime) = sprintf "%.0f" ((a - b).TotalMinutes)


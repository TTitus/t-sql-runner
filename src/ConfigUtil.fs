﻿module ConfigUtil

type UserConfig = {
    queryfilePath: string;
    serverDbConfigFilePath: string;
    indexHtmlOutputPath: string;
    throttleMilliseconds: int;
}


# T-SQL Runner

## Setup:

1. Put the solution directory SqlRunner in your home directory (~).

2. E.g. it expects this here:
	```fs
	let home = System.Environment.GetFolderPath(System.Environment.SpecialFolder.UserProfile) 
	let root = Path.Combine(home, "SqlRunner", "files")
	let queryfilePath = Path.Combine(root, "query.sql")
	let serverDbConfigFilePath = Path.Combine(root, "serverDbConfig.txt")
	let indexHtmlOutputPath = Path.Combine(root, "index.html")
	```

3. `npm config set strict-ssl false`

4. `npm i -g simple-hot-reload-server`
	
	https://www.npmjs.com/package/simple-hot-reload-server




## Usage

1) run this F# sln

2) `cd ~/SqlRunner/files/`, then run `hrs` in the command line

3) open this link in your browser: http://localhost:8082/?dubug=true&reload=true

4) write queries to ~\SqlRunner\files\query.sql

5) the query will use the connection info inside ~/SqlRunner/files/serverDbConfig.txt.
	The server name is expected to be on the first line, and the database name on the second line.

6) if you happen to let loose some rogue connections, you can use this SQL code to find and kill them, although if you close the terminal, it looks like it automatically closes the connections

	```sql
	SELECT conn.session_id, host_name, program_name,
		nt_domain, login_name, connect_time, last_request_end_time 
		, datediff(minute, connect_time, getdate()) as connection_age_mins
		, datediff(minute, last_request_end_time, getdate()) as last_req_mins_ago
	FROM sys.dm_exec_sessions AS sess
	JOIN sys.dm_exec_connections AS conn
		ON sess.session_id = conn.session_id
	where login_name = system_user

	kill <session_id>

	-- program_name will be something like "Core .Net SqlClient Data Provider"
	```